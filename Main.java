import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.*;
import java.util.*;
import java.util.Scanner;
import java.util.Hashtable;
import java.util.stream.Collectors;

public class Main
{
    public static ArrayList get_seats_from_file(int class_type){
        ArrayList<String> seats = new ArrayList<String>();
        String file_name;
        int i=0;
        if (class_type==1)
            file_name="buisness.txt";
        else
            file_name="economy.txt";

        try {
          File myObj = new File(file_name);
          Scanner myReader = new Scanner(myObj);
          while (myReader.hasNextLine()) {
            String data = myReader.nextLine();
            seats.add(data.replaceAll("\\s",""));
            i+=1;
          }
          myReader.close();
        } catch (FileNotFoundException e) {
          System.out.println("An error occurred.");
          e.printStackTrace();
        }
        return seats;
    }
    
    public static void write_to_file(int class_type, ArrayList<String> seats)
    {
        String file_name;
        if (class_type==1)
            file_name="buisness.txt";
        else
            file_name="economy.txt";

        try {
          FileWriter myWriter = new FileWriter(file_name);
          for(int i=0;i<seats.size();i++){
              myWriter.write(seats.get(i)+'\n');
          }
          myWriter.close();
        } catch (IOException e) {
          System.out.println("An error occurred.");
          e.printStackTrace();
        }
    }
    
    public static int get_cost(char seat, int type, int extra){
        int cost = 0;
        if(type==1)
            cost = (1000+ (100*extra));
        else
            cost = (2000+ (200*extra));
        if(seat=='W')
            cost+=100;
        return cost;
    }
    
    public static Hashtable booking(int booking_class, String[] seats_needed, int extra){
        int flag=0, cost=0;
        Hashtable<Integer, String> hm = new Hashtable<Integer, String>();
        hm.put(1, String.join(" ", seats_needed));
        
        for(int i=0;i<seats_needed.length;i++){
            ArrayList<String> seats_after_booking = get_seats_from_file(booking_class);
            String[] arrOfStr = seats_needed[i].split("_");
            int row,col=0;
            row = Integer.parseInt(arrOfStr[0]);
            
            int temp = (int)arrOfStr[1].charAt(0);
            if(temp<=90 & temp>=65)
                col = temp-64;
            else if(temp<=122 & temp>=97)
                col = temp-96;
            if(seats_after_booking.get(row).charAt(col) == 'B'){
                flag=1;
            }
            else{
                cost += get_cost(seats_after_booking.get(row).charAt(col), booking_class, extra);
                StringBuilder s = new StringBuilder(seats_after_booking.get(row)); 
                s.setCharAt(col, 'B');
                seats_after_booking.set( row, String.valueOf(s) );
            }
            String[] arr = new String[5]; 
            if(flag==0)
                write_to_file(booking_class, seats_after_booking);
        }
        
        if(cost!=0){
            hm.put(2, String.valueOf(cost));
        }
        return hm;
    }

    public static ArrayList form_seats(int a, int b, int c, int n){

        ArrayList<String> seats1 = new ArrayList<String>();
        ArrayList<String> seats2 = new ArrayList<String>();
        String left="", mid="", right="";
        int i;
        for(i=0;i<a;i++)
            left+='M';
        for(i=0;i<b;i++)
            mid+='M';
        for(i=0;i<c;i++)
            right+='M';
            
        StringBuilder ls = new StringBuilder(left);
        StringBuilder ms = new StringBuilder(mid);
        StringBuilder rs = new StringBuilder(right);
        ls.setCharAt(0, 'W');
        ls.setCharAt(a-1, 'A');
        
            
        if(a==1){
            ls.setCharAt(0, 'W');
        }else{
            ls.setCharAt(0, 'W');
            ls.setCharAt(a-1, 'A');
        }
        if(b==1){
            ms.setCharAt(0, 'A');
        }else{
            ms.setCharAt(0, 'A');
            ms.setCharAt(b-1, 'A');
        }
        if(c==1){
            rs.setCharAt(0, 'A');
        }else{
            rs.setCharAt(0, 'A');
            rs.setCharAt(c-1, 'W');
        }
        
        for(i=0;i<n;i++){
            String res = ls+ " " + ms + " " + rs;
            seats1.add(res);
            seats2.add(res.replaceAll("\\s", ""));
        }

        return seats1;
    }
    
	public static void main(String[] args) {
	    try {
          FileWriter myWriter = new FileWriter("A114.txt");
          myWriter.write("Buisness : {2,3,2} 12");
          myWriter.write("Economy : {3,4,3} 10");
          myWriter.close();
        } catch (IOException e) {
          System.out.println("An error occurred.");
          e.printStackTrace();
        }
	    try {
          File myObj = new File("A114.txt");
          Scanner myReader = new Scanner(myObj);
          while (myReader.hasNextLine()) {
            String data = myReader.nextLine();
            System.out.println(data);
          }
          myReader.close();
        } catch (FileNotFoundException e) {
          System.out.println("An error occurred.");
          e.printStackTrace();
        }
		Main obj = new Main();
		ArrayList<String> b_seats = obj.form_seats(2,3,2,12);
		ArrayList<String> e_seats = obj.form_seats(3,4,3,10);
		write_to_file(1, b_seats);
		write_to_file(2, e_seats);
		ArrayList<Hashtable> bookingArray = new ArrayList<Hashtable>();
		int ind=0;
		Scanner sc= new Scanner(System.in);
		Scanner scc= new Scanner(System.in);
		while(true){
		    System.out.println("Enter 1-to choose flight A114, 0-to exit and see all bookings - ");
            int str= sc.nextInt();
            if(str==0){
                break;
            }
            else if(str==1){
                System.out.println("Enter Booking class\n1-Buisness\n2-Economy\nClass - ");
                int c= sc.nextInt();
                System.out.println("Enter seats (format - 6_F) : ");
                String s = scc.nextLine();
                String[] n_seats = s.split(" ");
                bookingArray.add(booking(c, n_seats, ind));
                ind+=1;
            }
		}
		
		for(int i = 0; i<bookingArray.size(); i++){
		    Hashtable<Integer, String> h = bookingArray.get(i);
		    String res = "Booking Id - " + String.valueOf(i) + "\nBooking seats - " + String.valueOf(h.get(1)) + "\n" + "Total Cost - " + String.valueOf(h.get(2)) + "\nFlight Id - A114\n";
		    if (h.get(2) == null)
		        res = "Booking Id - " + String.valueOf(i) + " Seat Already booked";
		    System.out.println(res);
        }
	}
}

